-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2015 at 01:25 PM
-- Server version: 5.6.11-log
-- PHP Version: 5.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b00587546`
--
CREATE DATABASE IF NOT EXISTS `b00587546` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `b00587546`;

-- --------------------------------------------------------

--
-- Table structure for table `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `DirectorName` varchar(40) NOT NULL,
  `Age` tinyint(3) NOT NULL,
  `Nationality` varchar(40) NOT NULL,
  PRIMARY KEY (`DirectorName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `director`
--

INSERT INTO `director` (`DirectorName`, `Age`, `Nationality`) VALUES
(' Ben Affleck', 41, 'American'),
('Jeffrey Jacob Abrams', 47, 'American'),
('Judd Apatow', 46, 'American'),
('Michael Bay', 49, 'American'),
('Tim Burton', 55, 'American');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE IF NOT EXISTS `film` (
  `FilmName` varchar(60) NOT NULL,
  `FilmType` varchar(20) NOT NULL,
  `Length` varchar(9) NOT NULL,
  `DirectorName` varchar(30) NOT NULL,
  PRIMARY KEY (`FilmName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`FilmName`, `FilmType`, `Length`, `DirectorName`) VALUES
('Alice in Wonderland ', 'Fantasy', '108', 'Tim Burton'),
('Argo', 'Political Thriller', '120 Mins', 'Ben Affleck'),
('Charlie and the Chocolate Factory', ' fantasy', '115', 'Tim Burton'),
('Funny People', 'Comedy-Drama', '146', 'Judd Apatow'),
('Knocked Up', 'Romantic comedy', '129', 'Judd Apatow'),
('Mission: Impossible III ', 'Action ', '125', 'Jeffrey Jacob Abrams'),
('Pain & Gain ', 'Crime-Comedy', '129', 'Michael Bay'),
('Star Trek', 'Science Fiction', '127', 'Jeffrey Jacob Abrams'),
('The Town', 'crime drama ', '153', 'Ben Affleck'),
('Transformers', 'Science Fiction', '144', 'Michael Bay');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
